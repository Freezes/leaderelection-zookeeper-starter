# 使用指南

引入依赖

```
<dependency>
      <groupId>com.example</groupId>
      <artifactId>leaderelection-zookeeper-starter</artifactId>
      <version>1.0.1-SNAPSHOT</version>
</dependency>
```

配置如下

```
zookeeper.leader.election.zkAddress=localhost:2181
zookeeper.leader.election.instance=admin_culter
```

选主的逻辑

```
 public static void main(String[] args) throws IOException, InterruptedException, KeeperException {
     LeaderElection leaderElection = new LeaderElection();
     leaderElection.connect();
     leaderElection.runForLeader();
     leaderElection.close();
}
```

该示例代码实现了以下功能：

- 连接到ZooKeeper服务器。
- 创建选主目录`/election`（如果尚未创建）。
- 创建当前节点，作为竞选主

- 监视选主目录中的节点，并等待自己成为领导者。
- 如果自己不是最小的节点，则监视前一个节点。
- 如果前一个节点被删除，则重新等待自己成为领导者。

具体来说，该代码实现了以下步骤：

1. 在`connect`方法中，使用`ZooKeeper`类连接到ZooKeeper服务器。

2. 在`runForLeader`方法中，首先调用`createElectionNodeIfNotExists`方法来创建选主目录`/election`，然后调用`createCandidateNode`方法来创建当前节点。

   `createCandidateNode`方法将当前节点的路径保存在`currentZnodeName`变量中，并输出创建节点的路径。

3. 然后，该方法进入一个无限循环，其中它首先获取选主目录中的所有子节点，并对它们进行排序。然后，它检查当前节点是否是最小的节点。如果是，它将输出自己是领导者，并退出循环。否则，它将监视前一个节点，并等待其删除。

   如果前一个节点被删除，则触发`Watcher`的`process`方法。在该方法中，如果节点被删除，则重新等待自己成为领导者。

该示例代码仅仅是选举主节点的一个简单示例，并且没有考虑分布式系统中的复杂性和异常情况。实际的实现需要更多的工作来确保正确的行为和容错性。







