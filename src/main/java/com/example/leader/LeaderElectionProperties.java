package com.example.leader;

import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties(prefix = "zookeeper.leader.election")
public class LeaderElectionProperties {

    private String instance;

    private String zkAddress;

    public String getZkAddress() {
        return zkAddress;
    }


    public void setZkAddress(String zkAddress) {
        this.zkAddress = zkAddress;
    }


    public String getInstance() {
        return instance;
    }

    public void setInstance(String instance) {
        this.instance = instance;
    }


    @Override
    public String toString() {
        return "LeaderElectionProperties{" +
                "instance='" + instance + '\'' +
                ", zkAddress='" + zkAddress + '\'' +
                '}';
    }
}
